function Q=intquad(n)
C=ones(n);
Q=[C*-1,C*exp(1);C*pi,C];
end